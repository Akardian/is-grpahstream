package interfaces;

import java.util.regex.Pattern;

//"//(.*)\\s*(->)\\s*([^:\\s]*);"
public interface GraphRegex {
	static final String directed = "(.+?)(->)(.*?)(\\(.+?)?;";
	static final String undirected = "(.+?)(--)(.*?)(\\(.+?)?;";
	
	static final String directedWeightedGraph = "(.+?)(->)(.*?)(\\(.+?)?(:)(.+?);";
	static final String undirectedWeightedGraph = "(.+?)(--)(.*?)(\\(.+?)?(:)(.+?);";
	
	static final String specialNode = "(.+?)(\\(.+?)?;";

	public final Pattern REGEX = Pattern.compile("(.+?)(--|->)((.*?)((\\((.*?)\\)).*?)?((;)|(:)(.+?)(;)))|(.+?)(;)");
	
	//Gruppen der REGEX
	public final int NODE1_GROUPE = 1;
	
	public final int NODE2_GROUPE = 3;
	public final int EDGE_NAME_GROUPE = 4;
	public final int WEIGHT_GROUPE = 6;
}
