package interfaces;

public interface AttributeNames {
	public final String UI_LABEL = "ui.label";
	public final String UI_CLASS = "ui.class";
	public final String UI_STYLESHEET = "ui.stylesheet";
	
	public final String STYLE_EDGE = "edge";
	public final String STYLE_NODE = "node";
	public final String STYLE_NO_WEIGHT = STYLE_EDGE + ", noWeight";
	public final String STYLE_NODE_PATH = STYLE_NODE + ", path";
	public final String STYLE_EDGE_PATH = STYLE_EDGE + ", path";
	
	public final String NODE_NAME = "nodeName";
	
	public final String EDGE_NAME = "edgeName";
	public final String EDGE_WEIGHT = "length";
	
	public final String TRAV_TITEL = "title";
	public final String TRAV_PARENT = "parent";
	public final String TRAV_STEPS = "steps";
	
	public final String DIJKSTRA_TITEL = "title";
	public final String DIJKSTRA_PARENT = "precursor";
	public final String DIJKSTRA_DISTANCE = "dist";
	
	public final String FLOW_DIRECTION = "direction";
	public final String FLOW_CURRENT_CAPACITY = "currentCapacity";
}
