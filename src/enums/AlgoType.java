package enums;

public enum AlgoType {
	BFS, DFS, Dijkstra, Floyd, Ford, Karp
}
