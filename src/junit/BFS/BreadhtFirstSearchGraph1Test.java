package junit.BFS;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.traversing.BreadthFirstSearch;
import graph.GraphReader;

public class BreadhtFirstSearchGraph1Test {

	/*
	 * Es wird der Graph1 gelesen und der k�rzeste weg von "a" nach "g" gesucht 
	 */
	@Test
	public void test() {
		GraphReader graphReader = new GraphReader();
		
		File file = new File(graphReader.getAbsolutePath("/graphen/given/graph1.gka"));
		graphReader.initGraph(file);
		
		Node startNode = graphReader.getGraph().getNode("a");
		Node endNode = graphReader.getGraph().getNode("g");
		
		BreadthFirstSearch graphBFS = new BreadthFirstSearch(graphReader.getGraph(), startNode, endNode, false);
		
		graphBFS.run();
		
		List<Node> sPath = graphBFS.getShortestPath();
		System.out.println("shortestPath:" + sPath.toString());
		assertEquals("[a, k, g]", sPath.toString());
		System.out.println();
	}

}
