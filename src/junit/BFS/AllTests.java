package junit.BFS;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BreadhtFirstSearchGraph1NoReachTest.class, BreadhtFirstSearchGraph1Test.class,
		BreadhtFirstSearchGraph2Test.class, BreadhtFirstSearchGraph3Test.class })
public class AllTests {

}
