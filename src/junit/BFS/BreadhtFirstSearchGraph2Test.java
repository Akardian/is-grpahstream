package junit.BFS;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.traversing.BreadthFirstSearch;
import graph.GraphReader;

public class BreadhtFirstSearchGraph2Test {

	/*
	 *  Es wird der Graph2 gelesen und der k�rzeste weg von "a" nach "g" gesucht 
	 */
	@Test
	public void test() {
		GraphReader graphReader = new GraphReader();
		
		File file = new File(graphReader.getAbsolutePath("/graphen/given/graph2.gka"));
		graphReader.initGraph(file);
		
		Node startNode = graphReader.getGraph().getNode("b");
		Node endNode = graphReader.getGraph().getNode("d");
		
		BreadthFirstSearch graphBFS = new BreadthFirstSearch(graphReader.getGraph(), startNode, endNode, false);
		
		graphBFS.run();
		
		List<Node> sPath = graphBFS.getShortestPath();
		System.out.println("shortestPath:" + sPath.toString());
		assertEquals("[b, g, d]", sPath.toString());
		System.out.println();
	}

}
