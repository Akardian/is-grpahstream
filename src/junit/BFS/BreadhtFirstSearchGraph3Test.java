package junit.BFS;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.traversing.BreadthFirstSearch;
import graph.GraphReader;

public class BreadhtFirstSearchGraph3Test {

	/*
	 * Es wird der k�rzeste weg von L�beck nach Husum gesucht
	 */
	@Test
	public void test() {
		GraphReader graphReader = new GraphReader();

		File file = new File(graphReader.getAbsolutePath("/graphen/given/graph3.gka"));
		graphReader.initGraph(file);

		Node startNode = graphReader.getGraph().getNode("L�beck");
		Node endNode = graphReader.getGraph().getNode("Husum");

		BreadthFirstSearch graphBFS = new BreadthFirstSearch(graphReader.getGraph(), startNode, endNode, false);

		graphBFS.run();

		List<Node> sPath = graphBFS.getShortestPath();
		System.out.println("shortestPath:" + sPath.toString());
		assertEquals("[L�beck, L�neburg, Soltau, Hameln, Uelzen, Kiel, Husum]", sPath.toString());
		System.out.println();
	}

}
