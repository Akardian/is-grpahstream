package junit.BFS;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.traversing.BreadthFirstSearch;
import graph.GraphReader;

public class BreadhtFirstSearchGraph1NoReachTest {

	@Test
	public void test() {
		GraphReader graphReader = new GraphReader();
		
		File file = new File(graphReader.getAbsolutePath("/graphen/given/graph1.gka"));
		graphReader.initGraph(file);
		
		Node startNode = graphReader.getGraph().getNode("a");
		Node endNode = graphReader.getGraph().getNode("i");
		
		BreadthFirstSearch graphBFS = new BreadthFirstSearch(graphReader.getGraph(), startNode, endNode, false);
		
		graphBFS.run();
		
		List<Node> sPath = graphBFS.getShortestPath();
		System.out.println("shortestPath:" + sPath.toString());
		assertEquals("[]", sPath.toString());
		System.out.println();
	}

}
