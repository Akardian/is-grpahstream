package junit;

import java.util.ArrayList;
import java.util.List;

import org.graphstream.algorithm.APSP;
import org.graphstream.algorithm.APSP.APSPInfo;
import org.graphstream.algorithm.Dijkstra;
import org.graphstream.algorithm.flow.EdmondsKarpAlgorithm;
import org.graphstream.algorithm.flow.FordFulkersonAlgorithm;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import interfaces.AttributeNames;

public class GraphstreamAlgos implements AttributeNames{

	public List<Node> runDdijkstra(Graph graph, String startNode, String endNode) {
		// Edge lengths are stored in an attribute called "length"
			// The length of a path is the sum of the lengths of its edges
			Dijkstra dijkstra = new Dijkstra(Dijkstra.Element.EDGE, null, "length");

			// Compute the shortest paths in g from A to all nodes
			dijkstra.init(graph);
			dijkstra.setSource(graph.getNode(startNode));
			dijkstra.compute();

			// Build a list containing the nodes in the shortest path from A to B
			// Note that nodes are added at the beginning of the list
			// because the iterator traverses them in reverse order, from B to A
			List<Node> list = new ArrayList<Node>();
			for (Node node : dijkstra.getPathNodes(graph.getNode(endNode)))
				list.add(0, node);

			// cleanup to save memory if solutions are no longer needed
			dijkstra.clear();
			
			return list;
	}
	
	public List<Node> runFloydWarshall(Graph graph, String startNode, String endNode, boolean isDirected) { 		 		
 		if(graph == null) {
 			return null;
 		}
		APSP apsp = new APSP();
 		apsp.init(graph); // registering apsp as a sink for the graph
 		apsp.setDirected(isDirected); // undirected graph
 		apsp.setWeightAttributeName(EDGE_WEIGHT); // ensure that the attribute name used is "weight"
 
 		apsp.compute(); // the method that actually computes shortest paths
 		
 		APSPInfo info = graph.getNode(startNode).getAttribute(APSPInfo.ATTRIBUTE_NAME);
		
 		return info.getShortestPathTo(endNode).getNodePath();
	}
	
	public Double runFordFulkerson(Graph graph, Node startNode, Node endNode) { 		 		
 		if(graph == null) {
 			return null;
 		}
 		FordFulkersonAlgorithm fordFulkerson = new FordFulkersonAlgorithm();
 		fordFulkerson.init(graph, startNode.getId(), endNode.getId());
 		fordFulkerson.setCapacityAttribute(EDGE_WEIGHT);
 		
 		fordFulkerson.compute(); 

 		
 		return fordFulkerson.getMaximumFlow();
	}
	
	public Double runEdmondsKarp(Graph graph, Node startNode, Node endNode) { 		 		
 		if(graph == null) {
 			return null;
 		}
 		EdmondsKarpAlgorithm edmondsKarp = new EdmondsKarpAlgorithm();
 		edmondsKarp.init(graph, startNode.getId(), endNode.getId());
 		edmondsKarp.setCapacityAttribute(EDGE_WEIGHT);
 		
 		edmondsKarp.compute(); 

 		
 		return edmondsKarp.getMaximumFlow();
	}
	
	
	public Double getLenght(List<Node> list) {
		Double lenght = 0.0;
		for (int i = 0; i < list.size()-1; i++) {
			if(list.get(i).hasEdgeToward(list.get(i+1)) && list.get(i).getEdgeToward(list.get(i+1)).hasAttribute(EDGE_WEIGHT)) {
				Double currentLenght = list.get(i).getEdgeToward(list.get(i+1)).getAttribute(EDGE_WEIGHT);
				//System.out.println(currentLenght);
				lenght = lenght + currentLenght;
			} else {
				return Double.NaN;
			}
		}
		
		return lenght; 
	}
}
