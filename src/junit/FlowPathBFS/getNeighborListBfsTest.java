package junit.FlowPathBFS;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.maximumFlow.FlowPathBFS;
import graph.GraphReader;
import interfaces.AttributeNames;

public class getNeighborListBfsTest implements AttributeNames{

	@Test
	public void test() {
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/given/graph4 Directed.gka"));
		graph.initGraph(file);
		
		Node startNode = graph.getGraph().getNode("q");
		Node endNode = graph.getGraph().getNode("q");
		
		FlowPathBFS neighborList = new FlowPathBFS(graph.getGraph(), startNode, endNode, false);
		Node node = graph.getGraph().getNode("v3");
		
		//Init Edges for test
		Collection<Edge> edgeSet = graph.getGraph().getEdgeSet();
		Double capacity = 0.0;
		
		for (Edge edge : edgeSet) {
			edge.addAttribute(FLOW_CURRENT_CAPACITY, capacity);
			edge.removeAttribute(FLOW_DIRECTION);
		}
		
		List<Node> list = neighborList.getNeighborList(node);
		
		
		List<Node> correctList = new ArrayList<Node>();
		correctList.add(graph.getGraph().getNode("s"));
		correctList.add(graph.getGraph().getNode("v1"));
		//correctList.add(graph.getGraph().getNode("v6"));
		//correctList.add(graph.getGraph().getNode("v4"));		
		
		String check = "";
		check = list.toString();
		check = check + " Node: " + node;
		
		String correct = "";
		correct = correctList.toString();
		correct = correct + " Node: " + node;;

		System.out.println(className + ": " + check);
		System.out.println(className + ": " + correct);

		assertEquals(correctList, list);
		//assertEquals(gsList.size(), myList.size());
	}

}
