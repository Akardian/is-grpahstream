package junit.APSP;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ emptyGraphTest.class, Graph3HHtoHusum.class, Graph3Test.class, noPathTest.class, simpeTest.class })
public class AllTests {

}
