package junit.APSP;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.shortesPath.FloydWarshall;
import graph.GraphReader;
import junit.GraphstreamAlgos;

public class Graph3HHtoHusum {

	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/given/graph3.gka"));
		graph.initGraph(file);
		
		List<Node> gsList = new ArrayList<Node>(); 		
		gsList = new GraphstreamAlgos().runFloydWarshall(graph.getGraph(), "Hamburg", "Husum", true);

		Node sNode = graph.getGraph().getNode("Hamburg");
		Node eNode = graph.getGraph().getNode("Husum");
		FloydWarshall myAlgo = new FloydWarshall(graph.getGraph(), sNode, eNode);
		myAlgo.run();
		
		List<Node> myList = myAlgo.shortestPath();
		
		Double gsLength =  new GraphstreamAlgos().getLenght(gsList);
		Double myLength = new GraphstreamAlgos().getLenght(myList);
		
		String check = "";
		check = gsList.toString();
		check = check + " Length : " + gsLength;
		
		String correct = "";
		correct = myList.toString();
		correct = correct + " Length : " + myLength;

		System.out.println(className + ": " + correct);
		System.out.println(className + ": " + check);

		assertEquals(gsLength, myLength);
		//assertEquals(gsList.size(), myList.size());
	}

}
