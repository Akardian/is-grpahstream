package junit;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.maximumFlow.EdmondsKarp;
import algorithm.maximumFlow.FordFulkerson;
import enums.GraphType;
import graph.GraphCreator;

public class FordVsKarpBigTest {

	@Test
	public void test() {
		String className = this.getClass().getName();
		
		Double fordFlow = 0.0;
		Double karpFlow = 0.0;
		
		GraphCreator graphCreated = new GraphCreator(800, 300000, GraphType.DIRECTEDWEIGHTED, 10000);
		Graph graph = graphCreated.generateRandomNetwork();
		
		Node startNode = graph.getNode("q");
		Node endNode = graph.getNode("s");
		
		final long timeStartFord = System.nanoTime();
		FordFulkerson fordAlgo = new FordFulkerson(graph, startNode, endNode);
		fordFlow = fordFlow + fordAlgo.run();
		final long timeEndFord = System.nanoTime() - timeStartFord;
		final long timeFord = TimeUnit.MILLISECONDS.convert(timeEndFord, TimeUnit.NANOSECONDS);
		
		final long timeStartKarp = System.nanoTime();
		EdmondsKarp karpAlgo = new EdmondsKarp(graph, startNode, endNode);
		karpFlow = karpFlow + karpAlgo.run();
		final long timeEndKarp = System.nanoTime() - timeStartKarp;
		final long timeKarp = TimeUnit.MILLISECONDS.convert(timeEndKarp, TimeUnit.NANOSECONDS);
			
		System.out.println(className + ": " + " Werte[fordAlgo " + fordFlow + " Time: " +timeFord + "ms][karpAlgo " + karpFlow +" Time: " + timeKarp + "ms]");
		assertEquals(fordFlow,karpFlow, 0.0001);
	}

}
