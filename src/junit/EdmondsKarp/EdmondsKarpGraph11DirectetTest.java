package junit.EdmondsKarp;

import static org.junit.Assert.assertEquals;

import java.io.File;
import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.maximumFlow.EdmondsKarp;
import graph.GraphReader;
import junit.GraphstreamAlgos;

public class EdmondsKarpGraph11DirectetTest {

	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/given/graph11.gka"));
		graph.initGraph(file);
		
		Node startNode = graph.getGraph().getNode("b");
		Node endNode = graph.getGraph().getNode("g");
		
		Double gsFlow = new GraphstreamAlgos().runEdmondsKarp(graph.getGraph(), startNode, endNode);
		
		EdmondsKarp myAlgo = new EdmondsKarp(graph.getGraph(), startNode, endNode);
		Double myFlow = myAlgo.run();
		
		String check = "";
		//check = gsList.toString();
		check = check + " Flow my: " + myFlow;
		
		String correct = "";
		//correct = myList.toString();
		correct = correct + " Flow gs: " + gsFlow;

		System.out.println(className + ": " + check);
		System.out.println(className + ": " + correct);

		assertEquals(gsFlow, myFlow, 0.001);
		//assertEquals(gsList.size(), myList.size());
	}

}
