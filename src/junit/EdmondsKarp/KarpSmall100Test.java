package junit.EdmondsKarp;

import static org.junit.Assert.*;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.maximumFlow.EdmondsKarp;
import enums.GraphType;
import graph.GraphCreator;
import junit.GraphstreamAlgos;

public class KarpSmall100Test {

	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		
		Double gsFlow = 0.0;
		Double karpFlow = 0.0;
		
		for(int i =  0; i < 100; i++) {
			GraphCreator graphCreated = new GraphCreator(50, 200, GraphType.DIRECTEDWEIGHTED, 10);
			Graph graph = graphCreated.generateRandomNetwork();
			
			Node startNode = graph.getNode("q");
			Node endNode = graph.getNode("s");
			
			gsFlow = gsFlow + new GraphstreamAlgos().runEdmondsKarp(graph, startNode, endNode);
			
			EdmondsKarp karpAlgo = new EdmondsKarp(graph, startNode, endNode);
			karpFlow = karpFlow + karpAlgo.run();
			
		}
		System.out.println(className + ": " + "Werte[Ford " + gsFlow + "][Karp " + karpFlow +"]");
		assertEquals(gsFlow,karpFlow, 0.0001);
	}
}
