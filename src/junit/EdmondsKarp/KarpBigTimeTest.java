package junit.EdmondsKarp;

import static org.junit.Assert.*;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.maximumFlow.EdmondsKarp;
import enums.GraphType;
import graph.GraphCreator;
import graph.GraphReader;
import junit.GraphstreamAlgos;

public class KarpBigTimeTest {

	@Test
	public void test() {
		String className = this.getClass().getName();
		
		Double gsFlow = 0.0;
		Double karpFlow = 0.0;
		
		GraphCreator graphCreated = new GraphCreator(798, 300000, GraphType.DIRECTEDWEIGHTED, 10);
		Graph graph = graphCreated.generateRandomNetwork();
		
		Node startNode = graph.getNode("q");
		Node endNode = graph.getNode("s");
		
		final long timeStartGs = System.nanoTime();
		gsFlow =new GraphstreamAlgos().runEdmondsKarp(graph, startNode, endNode);
		final long timeEndGs = System.nanoTime() - timeStartGs;
		final long timeGs = TimeUnit.MILLISECONDS.convert(timeEndGs, TimeUnit.NANOSECONDS);
		
		final long timeStartKarp = System.nanoTime();
		EdmondsKarp karpAlgo = new EdmondsKarp(graph, startNode, endNode);
		karpFlow = karpFlow + karpAlgo.run();
		final long timeEndKarp = System.nanoTime() - timeStartKarp;
		final long timeKarp = TimeUnit.MILLISECONDS.convert(timeEndKarp, TimeUnit.NANOSECONDS);
			
		System.out.println(className + ": " + " Werte[Graphstream-Flow " + gsFlow + " Time: " +timeGs + "ms][Karp-Flow " + karpFlow +" Time: " + timeKarp + "ms]");
		assertEquals(gsFlow,karpFlow, 0.0001);
	}
}
