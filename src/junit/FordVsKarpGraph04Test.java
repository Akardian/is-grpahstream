package junit;

import static org.junit.Assert.*;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.maximumFlow.EdmondsKarp;
import algorithm.maximumFlow.FordFulkerson;
import graph.GraphReader;

public class FordVsKarpGraph04Test {

	@Test
	public void test() {
		String className = this.getClass().getName();
		
		Double fordFlow = 0.0;
		Double karpFlow = 0.0;
		
		GraphReader graph = new GraphReader();
		File file = new File(graph.getAbsolutePath("/graphen/given/graph4 Directed.gka"));
		graph.initGraph(file);
		
		Node startNode = graph.getGraph().getNode("q");
		Node endNode = graph.getGraph().getNode("s");
		
		final long timeStartFord = System.nanoTime();
		FordFulkerson fordAlgo = new FordFulkerson(graph.getGraph(), startNode, endNode);
		fordFlow = fordFlow + fordAlgo.run();
		final long timeEndFord = System.nanoTime() - timeStartFord;
		final long timeFord = TimeUnit.MILLISECONDS.convert(timeEndFord, TimeUnit.NANOSECONDS);
		
		final long timeStartKarp = System.nanoTime();
		EdmondsKarp karpAlgo = new EdmondsKarp(graph.getGraph(), startNode, endNode);
		karpFlow = karpFlow + karpAlgo.run();
		final long timeEndKarp = System.nanoTime() - timeStartKarp;
		final long timeKarp = TimeUnit.MILLISECONDS.convert(timeEndKarp, TimeUnit.NANOSECONDS);
			
		System.out.println(className + ": " + " Werte[fordAlgo " + fordFlow + " Time: " +timeFord + "ms][karpAlgo " + karpFlow +" Time: " + timeKarp + "ms]");
		assertEquals(fordFlow,karpFlow, 0.0001);
	}

}
