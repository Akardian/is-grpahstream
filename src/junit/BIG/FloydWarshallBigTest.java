package junit.BIG;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.shortesPath.Dijkstra;
import algorithm.shortesPath.FloydWarshall;
import graph.GraphReader;
import junit.GraphstreamAlgos;

public class FloydWarshallBigTest {

	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/given/BIG.gka"));
		graph.initGraph(file);
		
		List<Node> gsList = new ArrayList<Node>(); 		
		gsList = new GraphstreamAlgos().runFloydWarshall(graph.getGraph(), "N1", "N100", true);

		Node sNode = graph.getGraph().getNode("N1");
		Node eNode = graph.getGraph().getNode("N100");
		FloydWarshall myAlgo = new FloydWarshall(graph.getGraph(), sNode, eNode);
		myAlgo.run();
		
		List<Node> myList = myAlgo.shortestPath();
		
		Double gsLength =  new GraphstreamAlgos().getLenght(gsList);
		Double myLength = new GraphstreamAlgos().getLenght(myList);
		
		String check = "";
		check = gsList.toString();
		check = check + " Length : " + gsLength;
		
		String correct = "";
		correct = myList.toString();
		correct = correct + " Length : " + myLength;

		System.out.println(className + ": " + correct);
		System.out.println(className + ": " + check);

		assertEquals(gsLength, myLength);
		assertEquals(gsList.size(), myList.size());
	}
}