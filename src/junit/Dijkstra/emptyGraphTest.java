package junit.Dijkstra;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.shortesPath.Dijkstra;
import algorithm.shortesPath.FloydWarshall;
import graph.GraphReader;
import junit.GraphstreamAlgos;

public class emptyGraphTest {

	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/test/graphReader/emptyFileTest.gka"));
		graph.initGraph(file);
		
		List<Node> gsList = null; 

		Node sNode = graph.getGraph().getNode("Paderborn");
		Node eNode = graph.getGraph().getNode("Oldenburg");
		Dijkstra myAlgo = new Dijkstra(graph.getGraph(), sNode, eNode);
		myAlgo.run();
		
		List<Node> myList = myAlgo.shortestPath();
		
		String check = "";
		
		String correct = "";

		System.out.println(className + ": " + correct);
		System.out.println(className + ": " + check);

		assertEquals(gsList, myList);
	}

}
