package junit.generator;

import static org.junit.Assert.*;

import java.io.File;

import org.graphstream.algorithm.generator.Generator;
import org.graphstream.algorithm.generator.RandomGenerator;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.MultiGraph;
import org.junit.Test;

import enums.GraphType;
import graph.GraphReader;

public class randomGraph1000Test {

	/*
	 * Erzeugt einen zufälligen Grahpen mit 1000 Knoten 
	 */
	@Test
	public void test() {
		String className = this.getClass().getName();

		Graph rGraph = new MultiGraph("Random");
		Generator gen = new RandomGenerator(2);
		gen.addSink(rGraph);
		gen.begin();
		for (int i = 0; i < 1000; i++)
			gen.nextEvents();
		gen.end();

		GraphReader graph = new GraphReader();
		graph.setGraph(rGraph);
		graph.setGraphType(GraphType.UNDIRECTED);

		File savefile = new File(graph.getAbsolutePath("/graphen/test/randomGraph/randomGraph1000Test.gka"));
		graph.saveGraph(savefile);

		System.out.println(className + ": ");
		assertEquals(true, true);
		System.out.println();
	}

}
