package junit.generator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ randomGraph1000Test.class, randomGraph50Test.class })
public class AllTests {

}
