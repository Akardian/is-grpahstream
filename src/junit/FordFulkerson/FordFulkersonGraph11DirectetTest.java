package junit.FordFulkerson;

import static org.junit.Assert.assertEquals;

import java.io.File;
import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.maximumFlow.FordFulkerson;
import graph.GraphReader;
import junit.GraphstreamAlgos;

public class FordFulkersonGraph11DirectetTest {

	/*
	 * Test des kompletten FordFulkerson Algos
	 * Maximum Flow wird von b nach g gesucht und das Ergebnis mit dem Graphstream Algo überprüft
	 */
	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/given/graph11.gka"));
		graph.initGraph(file);
		
		Node startNode = graph.getGraph().getNode("b");
		Node endNode = graph.getGraph().getNode("g");
		
		Double gsFlow = new GraphstreamAlgos().runFordFulkerson(graph.getGraph(), startNode, endNode);
		
		FordFulkerson myAlgo = new FordFulkerson(graph.getGraph(), startNode, endNode);
		Double myFlow = myAlgo.run();
		
		String check = "";
		//check = gsList.toString();
		check = check + " Flow my: " + myFlow;
		
		String correct = "";
		//correct = myList.toString();
		correct = correct + " Flow gs: " + gsFlow;

		System.out.println(className + ": " + correct);
		System.out.println(className + ": " + check);
		
		assertEquals(gsFlow, myFlow, 0.001);
	}

}
