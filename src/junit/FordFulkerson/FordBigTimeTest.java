package junit.FordFulkerson;

import static org.junit.Assert.*;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.maximumFlow.FordFulkerson;
import enums.GraphType;
import graph.GraphCreator;
import graph.GraphReader;
import junit.GraphstreamAlgos;

public class FordBigTimeTest {

	/*
	 * Sehr lange laufzeit
	 */
	@Test
	public void test() {
		String className = this.getClass().getName();
		
		Double gsFlow = 0.0;
		Double fordFlow = 0.0;
		
		GraphCreator graphCreated = new GraphCreator(798, 300000, GraphType.DIRECTEDWEIGHTED, 10);
		Graph graph = graphCreated.generateRandomNetwork();
		
		Node startNode = graph.getNode("q");
		Node endNode = graph.getNode("s");
		
		final long timeStartGs = System.nanoTime();
		gsFlow =new GraphstreamAlgos().runEdmondsKarp(graph, startNode, endNode);
		final long timeEndGs = System.nanoTime() - timeStartGs;
		final long timeGs = TimeUnit.MILLISECONDS.convert(timeEndGs, TimeUnit.NANOSECONDS);
		
		final long timeStartKarp = System.nanoTime();
		FordFulkerson fordAlgo = new FordFulkerson(graph, startNode, endNode);
		fordFlow = fordFlow + fordAlgo.run();
		final long timeEndKarp = System.nanoTime() - timeStartKarp;
		final long timeKarp = TimeUnit.MILLISECONDS.convert(timeEndKarp, TimeUnit.NANOSECONDS);
			
		System.out.println(className + ": " + " Werte[Graphstream-Flow " + gsFlow + " Time: " +timeGs + "ms][fordFlow " + fordFlow +" Time: " + timeKarp + "ms]");
		assertEquals(gsFlow,fordFlow, 0.0001);
	}
}
