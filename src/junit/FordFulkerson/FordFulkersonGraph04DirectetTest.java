package junit.FordFulkerson;

import static org.junit.Assert.*;

import java.io.File;
import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.maximumFlow.FordFulkerson;
import graph.GraphReader;
import junit.GraphstreamAlgos;

public class FordFulkersonGraph04DirectetTest {

	/*
	 * Test des kompletten FordFulkerson Algos
	 * Maximum Flow wird von q nach s gesucht und das Ergebnis mit dem Graphstream Algo überprüft
	 */
	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/given/graph4 Directed.gka"));
		graph.initGraph(file);
		
		Node startNode = graph.getGraph().getNode("q");
		Node endNode = graph.getGraph().getNode("s");
		
		Double gsFlow = new GraphstreamAlgos().runFordFulkerson(graph.getGraph(), startNode, endNode);
		
		FordFulkerson myAlgo = new FordFulkerson(graph.getGraph(), startNode, endNode);
		Double myFlow = myAlgo.run();
		
		String check = "";
		//check = gsList.toString();
		check = check + " Flow my: " + myFlow;
		
		String correct = "";
		//correct = myList.toString();
		correct = correct + " Flow gs: " + gsFlow;

		System.out.println(className + ": " + correct);
		System.out.println(className + ": " + check);
		assertEquals(gsFlow, myFlow, 0.001);
	}

}
