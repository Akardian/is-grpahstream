package junit.maximumFlow;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.maximumFlow.FordFulkerson;
import graph.GraphReader;
import interfaces.AttributeNames;

public class MaximumFlowSetFlowTest implements AttributeNames{

	/*
	 * Test des Flow setzen
	 */
	@Test
	public void test() {
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/given/graph4 Directed.gka"));
		graph.initGraph(file);
		
		Node startNode = graph.getGraph().getNode("q");
		Node endNode = graph.getGraph().getNode("q");
		
		FordFulkerson fordFulkerson = new FordFulkerson(graph.getGraph(), startNode, endNode);
		
		//Path Nodes
		List<Node> path = new ArrayList<Node>();
		path.add(graph.getGraph().getNode("q"));
		path.add(graph.getGraph().getNode("v1"));
		path.add(graph.getGraph().getNode("v2"));
		path.add(graph.getGraph().getNode("s"));		
		
		List<Edge> edgeList = new ArrayList<>();
		//Get edges for Test
		edgeList.add(graph.getGraph().getEdge("qv1"));
		edgeList.add(graph.getGraph().getEdge("v2v1"));
		edgeList.add(graph.getGraph().getEdge("v2s"));
		//Set Edge CAPACITY
		graph.getGraph().getEdge("qv1").addAttribute(FLOW_CURRENT_CAPACITY, 4.0);
		graph.getGraph().getEdge("v2v1").addAttribute(FLOW_CURRENT_CAPACITY, 4.0);
		graph.getGraph().getEdge("v2s").addAttribute(FLOW_CURRENT_CAPACITY, 4.0);
		
		fordFulkerson.setFlow(4.0, path);
		
		List<Double> list = new ArrayList<>();
		for (Edge edge : edgeList) {
			list.add(edge.getAttribute(FLOW_CURRENT_CAPACITY));
		}
		
		List<Double> correctList = new ArrayList<>();
		correctList.add(8.0);
		correctList.add(0.0);
		correctList.add(8.0);
		
		String check = "";
		check = list.toString();
		check = check + "";
		
		String correct = "";
		correct = correctList.toString();
		correct = correct + "";

		System.out.println(className + ": " + check);
		System.out.println(className + ": " + correct);

		assertEquals(correctList, list);
	}

}
