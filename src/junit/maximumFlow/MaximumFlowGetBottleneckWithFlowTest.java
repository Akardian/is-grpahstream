package junit.maximumFlow;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.graphstream.graph.Node;
import org.junit.Test;

import algorithm.maximumFlow.FordFulkerson;
import graph.GraphReader;
import interfaces.AttributeNames;

public class MaximumFlowGetBottleneckWithFlowTest implements AttributeNames{

	/*
	 * Test der Bottelneck Erkennung
	 * Pfad ist nicht voll. Bottelneck ist 4
	 */
	@Test
	public void test() {
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/given/graph4 Directed.gka"));
		graph.initGraph(file);
		
		Node startNode = graph.getGraph().getNode("q");
		Node endNode = graph.getGraph().getNode("q");
		
		FordFulkerson fordFulkerson = new FordFulkerson(graph.getGraph(), startNode, endNode);
		
		List<Node> path = new ArrayList<Node>();
		path.add(graph.getGraph().getNode("q"));
		path.add(graph.getGraph().getNode("v1"));
		path.add(graph.getGraph().getNode("v2"));
		path.add(graph.getGraph().getNode("s"));
		
		//Init Edges
		graph.getGraph().getEdge("qv1").addAttribute(FLOW_CURRENT_CAPACITY, 0.0);
		graph.getGraph().getEdge("v2v1").addAttribute(FLOW_CURRENT_CAPACITY, 4.0);
		graph.getGraph().getEdge("v2s").addAttribute(FLOW_CURRENT_CAPACITY, 0.0);
		
		Double neck = fordFulkerson.getBottleneck(path);
		Double correctNeck = 4.0;
		
		String check = "";
		check = neck.toString();
		check = check + "";
		
		String correct = "";
		correct = correctNeck.toString();
		correct = correct + "";

		System.out.println(className + ": " + check);
		System.out.println(className + ": " + correct);

		assertEquals(correctNeck, neck, 0.001);
	}

}
