package junit.graphReader;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import graph.GraphReader;

public class ReadMixedGraphTypeTest {

	/*
	 * Es wird getestet ob bei gemischten Graphen nur eine Art hinzugefügt wird
	 */
	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/test/graphReader/mixedGraphTypeTest.gka"));
		graph.initGraph(file);

		String check;
		check = graph.getGraph().getNodeSet().toString();
		check = check + graph.getGraph().getEdgeSet().toString();

		System.out.println(className + ": " + check);

		assertEquals("[a, b, d -> c][ab[a--b]]", check);
		System.out.println();
	}

}
