package junit.graphReader;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import graph.GraphReader;

public class ReadWeightNoNumberParsTest {

	/*
	 * Es wird getestet ob ein falsches Gewicht geparst werden kann 
	 * und nicht als Gewicht übergeben wird
	 */
	
	@Test
	public void test() {
		//Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();
		
		File file = new File(graph.getAbsolutePath("/graphen/test/graphReader/WeightNoNumberParsTest.gka"));
		graph.initGraph(file);
		
		String check = graph.getGraph().getEdge("ab").getAttribute("weight");
		
		System.out.println(className+ ": " + check);
		
		assertNull(graph.getGraph().getEdge("ab").getAttribute("weight"));
		System.out.println();
	}

}
