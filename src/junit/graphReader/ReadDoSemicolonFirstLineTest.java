package junit.graphReader;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import graph.GraphReader;

public class ReadDoSemicolonFirstLineTest {

	/*
	 * Es wird getestet ob eine zeile ohne Semicolon gelesen werden kann ohen
	 * Probleme zu verursachen
	 */

	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/test/graphReader/noSemicolonFirstLineTest.gka"));
		graph.initGraph(file);

		String check = graph.getGraph().getEdgeSet().toString();
		//check = check + graph.getGraph().getEdge("ab");
		//check = check + graph.getGraph().getEdge("ab").getAttribute("weight");

		System.out.println(className + ": " + check);

		assertEquals("[]", check);
		System.out.println();
	}

}
