package junit.graphReader;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import graph.GraphReader;

public class ReadSimpleUnDirectedWeightedGraphTest {

	/*
	 * Testet ob ein einfacher gewichteter ungerichteter Graph eingelesen werden kann
	 * 
	 */
	
	@Test
	public void test() {
		//Parse Test Datei 
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();
		
		File file = new File(graph.getAbsolutePath("/graphen/test/graphReader/SimpleUnDirectedWeightedGraphTest.gka"));
		graph.initGraph(file);
				
		String check = graph.getGraph().getEdge("ab").toString();
		check =  check + graph.getGraph().getEdge("ab").getAttribute("weight");
		
		System.out.println(className+ ": " + check);				
		
		assertEquals("ab[a--b]10", check);
		System.out.println();
	}

}
