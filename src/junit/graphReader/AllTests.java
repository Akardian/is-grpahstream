package junit.graphReader;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ReadDoSemicolonFirstLineTest.class, ReadEmptyFileTest.class, ReadMixedGraphTypeTest.class,
		ReadNoSemicolonLastLineTest.class, ReadNoSemicolonSecondLineTest.class, ReadOneSingelNodeTest.class,
		ReadSimpleDirectedLabelTest.class, ReadSimpleDirectedTest.class, ReadSimpleDirectedWeightedGraphTest.class,
		ReadSimpleDirectedWeightedLabelTest.class, ReadSimpleUnDirectedLabelTest.class, ReadSimpleUnDirectedTest.class,
		ReadSimpleUnDirectedWeightedGraphTest.class, ReadWeightNoNumberParsTest.class })
public class AllTests {

}
