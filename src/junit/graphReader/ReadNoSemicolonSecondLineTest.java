package junit.graphReader;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import graph.GraphReader;

public class ReadNoSemicolonSecondLineTest {

	/*
	 * Es wird getestet ob ein Semicolon in der zweiten zeile fehlen kann
	 */
	
	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/test/graphReader/noSemicolonSecondLineTest.gka"));
		graph.initGraph(file);

		String check; 
		check = graph.getGraph().getNodeSet().toString();
		check = graph.getGraph().getEdgeSet().toString();
		
		System.out.println(className + ": " + check);

		assertEquals("[ab[a->b], ef[e->f]]", check);
		System.out.println();
	}

}
