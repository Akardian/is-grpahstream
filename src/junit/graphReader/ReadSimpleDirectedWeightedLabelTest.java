package junit.graphReader;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import graph.GraphReader;

public class ReadSimpleDirectedWeightedLabelTest {

	/*
	 * Es wird getestet ob das Label einer Kante richtig eingelesen wir bei
	 * einem gerichteten gwichteten Graphen
	 */
	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/test/graphReader/SimpleDirectedWeightedLabelTest.gka"));
		graph.initGraph(file);

		String check = graph.getGraph().getEdge("ab").getAttribute("ui.label");
		check = check + graph.getGraph().getEdge("ab");

		System.out.println(className + ": " + check);

		assertEquals("(ab)[10]ab[a->b]", check);
		System.out.println();
	}

}
