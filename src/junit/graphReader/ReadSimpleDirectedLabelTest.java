package junit.graphReader;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import graph.GraphReader;

/*
 * Es wird getester ob ein Label einer Kante bei einem gerichteten Graphen richtig eingelesen wird 
 */

public class ReadSimpleDirectedLabelTest {

	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/test/graphReader/SimpleDirectedLabelTest.gka"));
		graph.initGraph(file);

		String check = graph.getGraph().getEdge("ab").getAttribute("ui.label");
		check = check + graph.getGraph().getEdge("ab");
		check = check + graph.getGraph().getEdge("ab").getAttribute("weight");

		System.out.println(className + ": " + check);

		assertEquals("(ab)ab[a->b]null", check);
		System.out.println();
	}

}
