package junit.graphReader;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import graph.GraphReader;

public class ReadEmptyFileTest {

	/*
	 * Es wird getestet ob eine leere datei eingelesen werden kann
	 */
	
	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/test/graphReader/emptyFileTest.gka"));
		graph.initGraph(file);

		String check;
		check = graph.getGraph().getNodeSet().toString();

		System.out.println(className + ": " + check);

		assertEquals("[]", check);
		System.out.println();
	}

}
