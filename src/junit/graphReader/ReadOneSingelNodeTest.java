package junit.graphReader;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import graph.GraphReader;

public class ReadOneSingelNodeTest {

	/*
	 * Es wird getestet ob ein einzlner Knoten eingelesen werden kann
	 */
	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		File file = new File(graph.getAbsolutePath("/graphen/test/graphReader/oneSingelNodeTest.gka"));
		graph.initGraph(file);

		String check;
		check = graph.getGraph().getNodeSet().toString();

		System.out.println(className + ": " + check);

		assertEquals("[a]", check);
		System.out.println();
	}

}
