package junit.graphReader;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.channels.AsynchronousByteChannel;

import org.junit.Test;

import graph.GraphReader;

public class ReadSimpleDirectedTest {

	/*
	 * Es wird getestet ob ein einfacher gerichteter Graph ohne Kanten name und ohne Gewichtung gelesen werden kann
	 * 
	 */
	
	@Test
	public void test() {
		//Parse Test Datei 
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();
		
		File file = new File(graph.getAbsolutePath("/graphen/test/graphReader/SimpleDirectedTest.gka"));
		graph.initGraph(file);
				
		String check = graph.getGraph().getEdge("ab").toString();
		
		System.out.println(className+ ": " + check);				
		
		assertEquals("ab[a->b]", graph.getGraph().getEdge("ab").toString());
		System.out.println();
	}
}
