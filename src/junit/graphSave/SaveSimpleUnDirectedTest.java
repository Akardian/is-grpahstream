package junit.graphSave;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import enums.GraphType;
import graph.GraphReader;

public class SaveSimpleUnDirectedTest {

	/*
	 * Speichert eine einzelne ungerichtete Kante in einer Datei
	 */
	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();
		
		graph.getGraph().addNode("a");
		graph.getGraph().addNode("b");
		
		graph.setGraphType(GraphType.UNDIRECTED);
		graph.getGraph().addEdge("ab", "a", "b", false);

		File savefile = new File(graph.getAbsolutePath("/graphen/test/graphSave/SaveSimpleUnDirectedTest.gka"));
		graph.saveGraph(savefile);
		
		File file = new File(graph.getAbsolutePath("/graphen/test/graphSave/SaveSimpleUnDirectedTest.gka"));
		GraphReader cGraph = new GraphReader(); 
		
		cGraph.initGraph(file);

		String check = ""; 
		check = cGraph.getGraph().getNodeSet().toString();
		check = check + cGraph.getGraph().getEdgeSet().toString();
		
		String correct = ""; 
		correct = graph.getGraph().getNodeSet().toString();
		correct = correct + graph.getGraph().getEdgeSet().toString();

		System.out.println(className + ": " + check);

		assertEquals(correct, check);
		System.out.println();
	}

}
