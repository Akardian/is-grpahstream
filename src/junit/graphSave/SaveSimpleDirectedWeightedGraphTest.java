package junit.graphSave;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import enums.GraphType;
import graph.GraphReader;

public class SaveSimpleDirectedWeightedGraphTest {

	/*
	 * Speichert ein Gerichteten Gewichtete Kante in eine Datei
	 */
	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		graph.getGraph().addNode("a");
		graph.getGraph().addNode("b");

		graph.setGraphType(GraphType.DIRECTED);
		graph.getGraph().addEdge("ab", "a", "b", true);
		
		graph.getGraph().getEdge("ab").addAttribute("weight", "10");

		File savefile = new File(graph.getAbsolutePath("/graphen/test/graphSave/SaveSimpleDirectedWeightedGraphTest.gka"));
		graph.saveGraph(savefile);

		File file = new File(graph.getAbsolutePath("/graphen/test/graphSave/SaveSimpleDirectedWeightedGraphTest.gka"));
		GraphReader cGraph = new GraphReader();

		cGraph.initGraph(file);

		String check = "";
		check = cGraph.getGraph().getNodeSet().toString();
		check = check + cGraph.getGraph().getEdgeSet().toString();
		check = check + cGraph.getGraph().getEdge("ab").getAttribute("weight");

		String correct = "";
		correct = graph.getGraph().getNodeSet().toString();
		correct = correct + graph.getGraph().getEdgeSet().toString();
		correct = correct + graph.getGraph().getEdge("ab").getAttribute("weight");

		System.out.println(className + ": " + check);

		assertEquals(correct, check);
		System.out.println();
	}

}
