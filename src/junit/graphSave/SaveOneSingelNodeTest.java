package junit.graphSave;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import graph.GraphReader;

public class SaveOneSingelNodeTest {

	/*
	 * Speichert einen einzelnen Knoten in eine Datei
	 */
	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		graph.getGraph().addNode("a");

		//graph.setGraphType("");

		File savefile = new File(graph.getAbsolutePath("/graphen/test/graphSave/SaveOneSingelNodeTest.gka"));
		graph.saveGraph(savefile);

		File file = new File(graph.getAbsolutePath("/graphen/test/graphSave/SaveOneSingelNodeTest.gka"));
		GraphReader cGraph = new GraphReader();

		cGraph.initGraph(file);

		String check = "";
		check = cGraph.getGraph().getNodeSet().toString();
		check = check + cGraph.getGraph().getEdgeSet().toString();

		String correct = "";
		correct = graph.getGraph().getNodeSet().toString();
		correct = correct + graph.getGraph().getEdgeSet().toString();

		System.out.println(className + ": " + check);

		assertEquals(correct, check);
		System.out.println();
	}

}
