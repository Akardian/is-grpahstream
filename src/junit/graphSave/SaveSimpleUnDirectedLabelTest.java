package junit.graphSave;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import enums.GraphType;
import graph.GraphReader;

public class SaveSimpleUnDirectedLabelTest {

	/*
	 * Speichert eine ungerichtete Kante in eine Datei
	 */
	@Test
	public void test() {
		// Parse Test Datei
		String className = this.getClass().getName();
		GraphReader graph = new GraphReader();

		graph.getGraph().addNode("a");
		graph.getGraph().addNode("b");

		graph.setGraphType(GraphType.UNDIRECTED);
		graph.getGraph().addEdge("ab", "a", "b", false);

		graph.getGraph().getEdge("ab").addAttribute("edgeName", "(EdgeName)");

		File savefile = new File(graph.getAbsolutePath("/graphen/test/graphSave/SaveSimpleUnDirectedLabelTest.gka"));
		graph.saveGraph(savefile);

		File file = new File(graph.getAbsolutePath("/graphen/test/graphSave/SaveSimpleUnDirectedLabelTest.gka"));
		GraphReader cGraph = new GraphReader();

		cGraph.initGraph(file);

		String check = "";
		check = cGraph.getGraph().getNodeSet().toString();
		check = check + cGraph.getGraph().getEdgeSet().toString();
		check = check + cGraph.getGraph().getEdge("ab").getAttribute("edgeName");

		String correct = "";
		correct = graph.getGraph().getNodeSet().toString();
		correct = correct + graph.getGraph().getEdgeSet().toString();
		correct = correct + graph.getGraph().getEdge("ab").getAttribute("edgeName");

		System.out.println(className + ": " + check);

		assertEquals(correct, check);
		System.out.println();
	}

}
