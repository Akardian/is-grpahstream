package junit.graphSave;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SaveOneSingelNodeTest.class, SaveSimpleDirectedLabelTest.class, SaveSimpleDirectedTest.class,
		SaveSimpleDirectedWeightedGraphTest.class, SaveSimpleDirectedWeightedLabelTest.class,
		SaveSimpleUnDirectedLabelTest.class, SaveSimpleUnDirectedTest.class })
public class AllTests {

}
