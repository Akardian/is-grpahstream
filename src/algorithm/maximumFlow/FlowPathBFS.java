package algorithm.maximumFlow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import algorithm.traversing.BreadthFirstSearch;

public class FlowPathBFS extends BreadthFirstSearch {
	
	public FlowPathBFS(Graph graph, Node startNode, Node endNode, boolean SHOW_UI) {
		super(graph, startNode, endNode,SHOW_UI);
	}

	@Override
	public List<Node> getNeighborList(Node node) {		
		Collection<Edge> leavingEdges = node.getLeavingEdgeSet();
		Collection<Edge> enteringEdges = node.getEnteringEdgeSet();
		
		List<Node> nodeList = new ArrayList<>();
		
		for (Edge edge : leavingEdges) {
			if(edge.isDirected()) {
				Double currentCapacity = edge.getAttribute(FLOW_CURRENT_CAPACITY);
				Double maxCapacity = edge.getAttribute(EDGE_WEIGHT);
				
				if(currentCapacity < maxCapacity) {
					vistNeighbor(edge.getOpposite(node), node);
				}
			}
		}
		
		for (Edge edge : enteringEdges) {
			if(edge.isDirected()) {
				Double currentCapacity = edge.getAttribute(FLOW_CURRENT_CAPACITY);
	
				if(currentCapacity > 0.0) {
					vistNeighbor(edge.getOpposite(node), node);
				}
			}
		}
		return nodeList;
	}
	
	private void vistNeighbor(Node neighbor, Node node) {
		if(!getVisited().contains(neighbor)) {
			getQueue().add(neighbor);
			
			Integer steps = node.getAttribute(TRAV_STEPS);
			getVisited().add(setAttributes(neighbor, steps, node));
		}
	}
}