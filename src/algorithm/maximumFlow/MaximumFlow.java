package algorithm.maximumFlow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import interfaces.AttributeNames;

public abstract class MaximumFlow implements AttributeNames{
	private Node startNode, endNode;
	private Graph graph;
	
	public MaximumFlow(Graph graph, Node startNode, Node endNode) {
		this.graph = graph;
		this.startNode = startNode;
		this.endNode = endNode;
		
	}
	
	abstract protected List<Node> getPath();
	
	public double run() {
		Double maxFlow = Double.NaN; 
		if(graph != null && startNode != null && endNode != null && !startNode.equals(endNode)) {
			//1. Init			
			maxFlow = 0.0;
			initialize();
			
			List<Node> path = getPath();
			
			while(!(path.isEmpty())) {
				//3. Verg��erung der Flu�st�rke
				Double bottelneck = getBottleneck(path);
				setFlow(bottelneck, path);
				
				//4. maxFluss brerechnen
				maxFlow = maxFlow + bottelneck;
				
				//2. Pfad finden
				path = getPath();
			}
		}
		return maxFlow;
	}
	
	private Double getCapacity(Node node1, Node node2) {
		Edge edge = node1.getEdgeBetween(node2);
		Double capacity = 0.0;
		
		Double currentCapacity = edge.getAttribute(FLOW_CURRENT_CAPACITY);
		Double maxCapacity = edge.getAttribute(EDGE_WEIGHT);
		
		if(edge.getSourceNode().equals(node1)) {			
			capacity = maxCapacity - currentCapacity;
		} else {
			capacity = currentCapacity;
		}
		
		return capacity;
	}
	
	//TODO Nur public zum testen
	public Double getBottleneck(List<Node> nodeList) {
		Double maxFlow = Double.NaN;
		if(!nodeList.isEmpty()) {
			Node node1 = nodeList.get(0);
			
			for(int i = 1; i < nodeList.size(); i++) {
				Node node2 = nodeList.get(i);
				
				Double capacity = getCapacity(node1, node2);
				if(capacity <= maxFlow || maxFlow.isNaN()) {
					maxFlow = capacity;
				}
				
				node1 = node2;
			}
		}		
		return maxFlow;
	}
	
	//TODO Nur public zum testen
	public void setFlow(Double maxFlow, List<Node> nodeList) {
		if(!nodeList.isEmpty()) {
			Node node1 = nodeList.get(0);
			
			for(int i = 1; i < nodeList.size(); i++) {
				Node node2 = nodeList.get(i);
				
				Edge edge = node1.getEdgeBetween(node2);
				Double currentCapacity = edge.getAttribute(FLOW_CURRENT_CAPACITY);
				
				if(edge.getSourceNode().equals(node1)) {	
					currentCapacity = currentCapacity + maxFlow;
				} else {
					currentCapacity = currentCapacity - maxFlow;
				}
				edge.setAttribute(FLOW_CURRENT_CAPACITY, currentCapacity);
				
				//Update GUI
				Double weight = edge.getAttribute(EDGE_WEIGHT);
				String name = edge.getAttribute(EDGE_NAME);
				edge.setAttribute(UI_LABEL, name +"[" + weight + "] " + currentCapacity);
				
				node1 = node2;
			}
		}		
	}
	
	private void initialize() {
		Collection<Edge> edgeSet = graph.getEdgeSet();
		Double capacity = 0.0;
		
		for (Edge edge : edgeSet) {
			edge.addAttribute(FLOW_CURRENT_CAPACITY, capacity);
			edge.removeAttribute(FLOW_DIRECTION);
		}
	}

	public Node getStartNode() {
		return startNode;
	}

	public Node getEndNode() {
		return endNode;
	}

	public Graph getGraph() {
		return graph;
	}
	
	@Override
	public String toString() {
		return "MaximumFlow [startNode=" + startNode + ", endNode=" + endNode + ", graph=" + graph + "]";
	}
}