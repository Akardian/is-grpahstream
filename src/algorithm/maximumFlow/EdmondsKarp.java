package algorithm.maximumFlow;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import interfaces.AttributeNames;

public class EdmondsKarp extends MaximumFlow implements AttributeNames{
	long runTime = 0;
	long shortestPath = 0;
	
	public EdmondsKarp(Graph graph, Node startNode, Node endNode) {
		super(graph, startNode, endNode);
	}

	@Override
	protected List<Node> getPath() {
		FlowPathBFS bfs = new FlowPathBFS(getGraph(), getStartNode(), getEndNode(), false);
		
		final long timeStartRun = System.nanoTime();
		bfs.run();
		final long timeEndRun = System.nanoTime() - timeStartRun;
		final long timeRun = TimeUnit.MILLISECONDS.convert(timeEndRun, TimeUnit.NANOSECONDS);
		
		final long timeStartPath = System.nanoTime();
		List<Node> path = bfs.getShortestPath();
		final long timeEndPath = System.nanoTime() - timeStartPath;
		
		runTime = runTime + timeRun;
		shortestPath = shortestPath + timeEndPath;
		
		//System.out.println("Run:" + timeRun + "ms getPath: " + timeEndPath + "ns | EndTime Run:" + runTime + "ms getPath: " + shortestPath + "ns");
		
		return path;
	}
}
