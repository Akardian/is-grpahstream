package algorithm.shortesPath;

import java.util.Comparator;

import org.graphstream.graph.Node;

public class NodeComparator implements Comparator<Node> {

	@Override
	public int compare(Node arg0, Node arg1) {
		int compare = -1;

		if (arg0.hasAttribute("dist") && arg1.hasAttribute("dist")) {

			if (arg0.getAttribute("dist") instanceof Double && 
				arg1.getAttribute("dist") instanceof Double) {
				
				Double node0 = arg0.getAttribute("dist");
				Double node1 = arg1.getAttribute("dist");
			
				if (arg0 != null && arg1 != null) {
					if (node0 < node1) {
						compare = -1;
					} else if (node0 > node1) {
						compare = 1;
					} else {
						compare = 0;
					}
				}
			}
		}
		return compare;
	}
}
