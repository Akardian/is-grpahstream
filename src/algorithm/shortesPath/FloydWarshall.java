package algorithm.shortesPath;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import com.sun.xml.internal.ws.api.pipe.NextAction;

import interfaces.AttributeNames;

public class FloydWarshall implements AttributeNames{
	private Node startNode, endNode;
	private Graph graph;
	
	private List<Node> nodeList;
	private Double[][] distanzmatrix;
	private Node[][] transitmatrix;
	
	public FloydWarshall(Graph graph, Node startNode, Node endNode) {
		this.graph = graph;
		this.startNode = startNode;
		this.endNode = endNode;
		
		nodeList = new ArrayList<Node>();
		distanzmatrix = new Double[graph.getNodeCount()][graph.getNodeCount()];
		transitmatrix = new Node[graph.getNodeCount()][graph.getNodeCount()];
	}
	
	public void run() {
		if(graph == null) {
			return; //if nothing to calculate
		}

		
		/**
		 * Erstelle eine "feste" Liste mit allen Nodes
		 */
		for (Node node : graph.getNodeSet()) {
			nodeList.add(node);
		}
		
		initmatrix();
		runFloyd();
		
	}
	
	private void runFloyd() {
		for (int i = 0; i < distanzmatrix.length; i++) {
			Node from = nodeList.get(i);
			for (int j = 0; j < distanzmatrix[i].length; j++) {
				for (int k = 0; k < distanzmatrix[i].length; k++) {
					Double currMin = distanzmatrix[j][k];
					Double newMin = distanzmatrix[j][i] + distanzmatrix[i][k];
					
					if(currMin > newMin) {
						distanzmatrix[j][k] = newMin;
						transitmatrix[j][k] = from;
					}
				}	
			}
			if(distanzmatrix[i][i] < 0) {
				System.out.println("Kreis mit negativer Länge");
				return;
			}
		}
	}

	private void initmatrix() {
		for (int i = 0; i < distanzmatrix.length; i++) {
			Node from = nodeList.get(i);
			for (int j = 0; j < distanzmatrix[i].length; j++) {
				Node to = nodeList.get(j);
				
				if (i == j) { //der gleiche Knoten 
					distanzmatrix[i][j] = 0.0; //Setzte Distanz auf 0
					transitmatrix[i][j] = null; //Setzte sich selbst als vorgänger
				} else {
					if(from.hasEdgeToward(to)) { //Hat eine direkte Kante
						distanzmatrix[i][j] = from.getEdgeToward(to).getAttribute(EDGE_WEIGHT);
					} else { //hat keine direkte Kante
						distanzmatrix[i][j] = Double.POSITIVE_INFINITY;
					}
				}
			}
		}
	}
	
	public List<Node> shortestPath() {
		if(!nodeList.contains(startNode) || !nodeList.contains(endNode) ) {
              System.out.println("Node im Graph nicht gefunden");
              return null;
		}
		//System.out.println("von " + startNode + "->" + endNode + ": " + getPath(startNode, endNode));
		List<Node> path = getPath(startNode, endNode);
		path.add(0, startNode);
		path.add(endNode);
		
		return path; 
	}
	
	public List<Node> getPath(Node sNode, Node eNode) {
		List<Node> path = new ArrayList<Node>();
		Integer startNodeI = nodeList.indexOf(sNode);
		Integer endNodeI = nodeList.indexOf(eNode);
		
		Node nodeBtw = transitmatrix[startNodeI][endNodeI];
		if ( nodeBtw == null) {
			return path;
		} else {
System.out.println("von " + sNode + "->" + eNode + ": " + nodeBtw);		
			path.addAll(getPath(sNode, nodeBtw));
System.out.println("1 " + path);
			path.add(nodeBtw);
System.out.println("2 " + path);
			path.addAll(getPath(nodeBtw, eNode));
System.out.println("3 " + path);

			return path;
		}
	}

}
