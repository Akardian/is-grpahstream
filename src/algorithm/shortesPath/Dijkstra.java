package algorithm.shortesPath;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import interfaces.AttributeNames;

public class Dijkstra implements AttributeNames{
	private List<Node> vertexList;
	private Comparator<Node> comparator;
	
	private Node startNode, endNode;
	private Graph graph;
	
	public Dijkstra(Graph graph, Node startNode, Node endNode) {
		this.graph = graph;
		this.startNode = startNode;
		this.endNode = endNode;
		
		comparator = new NodeComparator();
		vertexList = new ArrayList<Node>();
		
	}
	
	public void run() {
		if(graph == null || startNode == null || endNode == null) {
			return; //if nothing to calculate
		}
		
		initialize();
		dijkstra();
	}

	private void initialize() {
		Collection<Node> allNodes = graph.getNodeSet();
		
		for(Node node: allNodes) {
			node.removeAttribute(DIJKSTRA_PARENT);
			
			if(node.equals(startNode)) {
				node.addAttribute(DIJKSTRA_DISTANCE, 0.0);
			} else {
				node.addAttribute(DIJKSTRA_DISTANCE, Double.POSITIVE_INFINITY);
			}
			vertexList.add(node);
		}
	}
	
	private void dijkstra() {
		while(!vertexList.isEmpty()) {			
			Node currentNode = vertexList.stream().min(comparator).get();
			
			vertexList.remove(currentNode);
			Double nodeDist = currentNode.getAttribute(DIJKSTRA_DISTANCE);
			
			Map<Node, Double> neighbor = getNeighbor(currentNode);
			neighbor.forEach((node, weight) -> {
				if(vertexList.contains(node)) {				
					Double currentDist = nodeDist + weight;
					Double oldDist = node.getAttribute(DIJKSTRA_DISTANCE);
					
					if(currentDist < oldDist) {
						setAttribute(node, currentNode, currentDist);
					}
				}
			});
		}
	}
	
	private Map<Node, Double> getNeighbor (Node node) {
		Iterable<Edge> leavingEdges = node.getEachLeavingEdge();
		Map<Node, Double> oppositeNodes = new HashMap<Node, Double>();
		
		for(Edge currentEdge : leavingEdges) {
			if(currentEdge.hasAttribute(EDGE_WEIGHT) ) {
				Double weight = currentEdge.getAttribute(EDGE_WEIGHT);
				
				if(weight.isNaN()) {
					currentEdge.addAttribute(UI_CLASS, STYLE_NO_WEIGHT);
				} else {
					if(currentEdge.isDirected()) {
						oppositeNodes.put(currentEdge.getTargetNode(), weight);
					} else {
						oppositeNodes.put(currentEdge.getOpposite(node), weight);
					}	
				}
				
			}
		}
		return oppositeNodes;
	}
	
	private void setAttribute(Node node, Node precursor, Double dist) {
		node.addAttribute(DIJKSTRA_PARENT, precursor);
		node.addAttribute(DIJKSTRA_DISTANCE, dist);
		node.setAttribute(UI_LABEL, node.getAttribute(NODE_NAME) + "["+ dist +"]");
	}
	
	public List<Node> shortestPath() {
		if(graph == null) {
			return null;
		}
		
		ArrayList<Node> path = new ArrayList<Node>();

		path.add(endNode);
		
		Node currentNode = endNode;
		
		while(  currentNode != null &&
				currentNode.hasAttribute(DIJKSTRA_PARENT) && 
				currentNode.getAttribute(DIJKSTRA_PARENT) != null) {
			
			currentNode = currentNode.getAttribute(DIJKSTRA_PARENT);

			path.add(currentNode);
		}
		
		if(currentNode != null && path.contains(startNode)) {
			Collections.reverse(path);
		} else {
			path = null;
		}	

		return path;
	}

}