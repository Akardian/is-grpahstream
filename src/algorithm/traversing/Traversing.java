package algorithm.traversing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import interfaces.AttributeNames;

public abstract class Traversing implements AttributeNames{
	private final boolean SHOW_UI;

	private List<Node> visited;
	
	final private Node startNode;
	final private Node endNode;
	private Graph graph;

	public Traversing(Graph graph, Node startNode, Node endNode,  boolean SHOW_UI) {
		this.graph = graph;
		this.startNode = startNode;
		this.endNode = endNode;
		this.SHOW_UI = SHOW_UI;
	}
	
	public abstract Integer run();
	public abstract List<Node> getNeighborList(Node node);
	
	public void initialize() {
		Collection<Node> nodeList = graph.getNodeSet();
		
		for (Node node : nodeList) {
			node.removeAttribute(TRAV_STEPS);
			node.removeAttribute(TRAV_PARENT);
			showUi(node,  "");
		}

		visited = new ArrayList<Node>();
		
		//Setzte Start Knoten "Steps"
		startNode.addAttribute(TRAV_STEPS, 0);
	}
	
	protected Node setAttributes(Node node, Integer steps, Node parent) {		
		node.addAttribute(TRAV_STEPS, steps + 1);
		showUi(node, "["+ (steps + 1) +"]");
		node.addAttribute(TRAV_PARENT, parent);
		
		return node;
	}
	
	public List<Node> getShortestPath() {
		ArrayList<Node> path = new ArrayList<Node>();

		Node currentNode = endNode;
		
		while(currentNode.hasAttribute(TRAV_PARENT)) {
			path.add(currentNode);
			currentNode = currentNode.getAttribute(TRAV_PARENT);
			
			if(currentNode.equals(startNode)) {
				path.add(startNode);
			}
		}

		if(path.contains(startNode)) {
			Collections.reverse(path);
		} else {
			path = new ArrayList<Node>();
		}	
		
		return path;
	}
	
	private void showUi(Node node, String string) {
		if(SHOW_UI) {
			node.addAttribute(UI_LABEL, node.getAttribute(NODE_NAME) + string);
		}
	}

	public List<Node> getVisited() {
		return visited;
	}

	public Node getStartNode() {
		return startNode;
	}

	public Node getEndNode() {
		return endNode;
	}

	public Graph getGraph() {
		return graph;
	}

	@Override
	public String toString() {
		return "Traversing [visited=" + visited + ", startNode=" + startNode + ", endNode=" + endNode + ", graph="
				+ graph + "]";
	}
}
