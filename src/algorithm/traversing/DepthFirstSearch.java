package algorithm.traversing;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import interfaces.AttributeNames;

public class DepthFirstSearch extends Traversing implements AttributeNames {
	
	private Stack<Node> stack; //markiert

	public DepthFirstSearch(Graph graph, Node startNode, Node endNode,  boolean SHOW_UI) {
		super(graph, startNode, endNode, SHOW_UI);
	}
	
	@Override
	public Integer run() {
		Integer steps = -1;		
		if(getGraph() != null && getStartNode() != null && getEndNode() != null) {
			initialize();
			stack = new Stack<Node>();
			
			stack.push(getStartNode());
			getVisited().add(getStartNode());
			
			// Solange bis Stack nicht leer ist
			while(!stack.isEmpty()) {
				Node topNode = stack.pop(); // erstes Element von der queue nehmen

				if(topNode.equals(getEndNode())) { //Ziel Knoten Erreicht
					stack.clear();
					steps = topNode.getAttribute(TRAV_STEPS);
				} else {
					getNeighborList(topNode);
				}

			}
		}
		return steps;
	}
	
	@Override
	public List<Node> getNeighborList(Node node) {
		Iterable<Edge> leavingEdges = node.getEachLeavingEdge();
		List<Node> nodeList = new ArrayList<>();
		
		for(Edge currentEdge : leavingEdges) {
			Node neighbor;
			
			if(currentEdge.isDirected()) {
				neighbor = currentEdge.getTargetNode();
			} else {
				neighbor = currentEdge.getOpposite(node);
			}
			
			//Nachbar ist noch nicht Markiert
			if(!getVisited().contains(neighbor)) {				
				stack.push(neighbor);
				
				Integer steps = node.getAttribute(TRAV_STEPS);
				getVisited().add(setAttributes(neighbor, steps, node));
			}
		}
		
		return nodeList;
	}

	public Stack<Node> getStack() {
		return stack;
	}

	@Override
	public String toString() {
		return "DepthFirstSearch [stack=" + stack + "]";
	}
}