package algorithm.traversing;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import interfaces.AttributeNames;

public class BreadthFirstSearch extends Traversing implements AttributeNames {
	
	private ArrayDeque<Node> queue;

	public BreadthFirstSearch(Graph graph, Node startNode, Node endNode, boolean SHOW_UI) {
		super(graph, startNode, endNode, SHOW_UI);
	}
	
	public Integer run() {
		Integer steps = -1;		
		if(getGraph() != null && getStartNode() != null && getEndNode() != null) {
			initialize();
			queue = new ArrayDeque<>();
		
			queue.offer(getStartNode());			
			getVisited().add(getStartNode());
			
			// Solange bis Stack nicht leer ist
			while(!queue.isEmpty()) {
				Node firstNode = queue.poll(); // erstes Element von der queue nehmen
				
				if(firstNode.equals(getEndNode())) { //Ziel Knoten Erreicht
					queue.clear();
					steps = firstNode.getAttribute(TRAV_STEPS);
				} else {
					getNeighborList(firstNode);
				}
			}
		}
		return steps;
	}
	
	@Override
	public List<Node> getNeighborList(Node node) {
		Iterable<Edge> leavingEdges = node.getEachLeavingEdge();
		List<Node> nodeList = new ArrayList<>();
		
		for(Edge currentEdge : leavingEdges) {
			Node neighbor;
			if(currentEdge.isDirected()) {
				neighbor = currentEdge.getTargetNode();
			} else {
				neighbor = currentEdge.getOpposite(node);
			}
			
			//Nachbar ist noch nicht Markiert
			if(!getVisited().contains(neighbor)) {
				queue.offer(neighbor);
				
				Integer steps = node.getAttribute(TRAV_STEPS);
				getVisited().add(setAttributes(neighbor, steps, node));
			}
		}
			
		return nodeList;
	}

	public Queue<Node> getQueue() {
		return queue;
	}

	@Override
	public String toString() {
		return "BreadthFirstSearch [queue=" + queue + "]";
	}
}