package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class Main extends Application {
	
	public static void main(String args[]) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("Screen.fxml"));
			Scene scene = new Scene(root, 600, 350);

			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("GKA - Praktikum 1");
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
