package gui;

import java.io.File;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.graphstream.graph.Node;
import enums.AlgoType;
import enums.GraphType;
import graph.GraphCreator;
import graph.GraphReader;
import graph.GraphViewer;
import gui.logik.GuiPathAlgorithm;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.stage.FileChooser;

public class MainController{

	@FXML
	private TextArea text_area;

	@FXML
	private Button save_btn;

	@FXML
	private Button open_btn;

	@FXML
	private Button algo_btn;
	
	@FXML
	private Button auto_btn;
	
	@FXML
	private Button gen_btn;

	@FXML
	private ChoiceBox<Node> endNode = new ChoiceBox<Node>();

	@FXML
	private ChoiceBox<Node> startNode = new ChoiceBox<Node>();

	@FXML
	private ToggleGroup algorithm;

	@FXML
	private RadioButton bfs_radio;
	
	@FXML
	private RadioButton dfs_radio;

	@FXML
	private RadioButton dijistra_radio;

	@FXML
	private RadioButton floydWarshall_radio;
	
	@FXML
    private RadioButton edmonds_radio;	
    
    @FXML
    private RadioButton ford_radio;
    
	@FXML
	private ChoiceBox<File> gkaFiles;

	private GraphReader graph = new GraphReader();
	private GraphViewer viewer = new GraphViewer();
	private boolean toggleAllignment = false;

	@FXML
	protected void initialize() {
		showFileList();

		openFolder();
		saveGraph();
		runAlgorithm();
	}

	private void showFileList() {
		gkaFiles.getItems().clear();

		File folder = new File("graphen/given");
		File[] listOfFiles = folder.listFiles();

		for (File file : listOfFiles) {
			if (file.isFile()) {
				gkaFiles.getItems().add(file);
			}
		}
		
		gkaFiles.getSelectionModel().selectedIndexProperty()
				.addListener((ObservableValue<? extends Number> ov, Number old_value, Number new_value) -> {
					openGraph(gkaFiles.getItems().get((int) new_value));
				});
	}
	
	private void openGraph(File f) {
		graph = new GraphReader();

		graph.initGraph(f);
		viewer.displayGraph(graph.getGraph());

		startNode.getItems().clear();
		endNode.getItems().clear();

		startNode.getItems().addAll(graph.getGraph().getNodeSet());
		endNode.getItems().addAll(graph.getGraph().getNodeSet());
	}
	
	
	
	@FXML
	public void openFolder() {
		open_btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				FileChooser chooser = new FileChooser();
				chooser.setTitle("Ausw�ｾ�ｽ､hlen von .gka Datei");
				File choosedFile = chooser.showOpenDialog(null);

				openGraph(choosedFile);

			}

		});

	}

	@FXML
	public void saveGraph() {
		save_btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				JFrame frame = new JFrame("Enter file name");
				String name = JOptionPane.showInputDialog(frame, "Enter file name");

				File tmp = new File(name);
				graph.saveGraph(tmp);

			}

		});
	}

	@FXML
	void runAlgorithm() {
		algo_btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				viewer.clean(graph.getGraph());

				if(graph.getGraphType() == null) {
					
					text_area.appendText("Kein Graph ausgewählt \n");
				} else {
				
					GuiPathAlgorithm algo = new GuiPathAlgorithm(graph, viewer);
					List<Node> path = null;
					Double myFlow = Double.NaN;
					Toggle s = algorithm.getSelectedToggle();				
					
					if (s.equals(bfs_radio)) {
						path = algo.runBFS(startNode, endNode);
						algo.printPathOnGui(path, text_area, AlgoType.BFS);
					} else if (s.equals(dfs_radio)) {
						path = algo.runDFS(startNode, endNode);
						algo.printPathOnGui(path, text_area, AlgoType.DFS);
					} else if (s.equals(dijistra_radio)) {
						path = algo.runDijkstra(startNode, endNode);
						algo.printPathOnGui(path, text_area, AlgoType.Dijkstra);
					} else if (s.equals(floydWarshall_radio)) {
						path = algo.runFloyd(startNode, endNode);
						algo.printPathOnGui(path, text_area, AlgoType.Floyd);
					} else if (s.equals(edmonds_radio)) {
						myFlow = algo.runEdmondsKarp(startNode, endNode);
						text_area.appendText("Max Flow: " + myFlow + " \n");
					} else if (s.equals(ford_radio)) {
						myFlow = algo.runFord(startNode, endNode);
						text_area.appendText("Max Flow: " + myFlow + " \n");
					} else {
						text_area.appendText("not implemented yet");
					}
				}
			}
		});
	}
	
	@FXML
	void genGraph() {
		GraphCreator gc = new GraphCreator(50, 200, GraphType.DIRECTEDWEIGHTED, 50);
		graph = new GraphReader();

		graph.setGraph(gc.generateRandomGraph());
		graph.setGraphType(GraphType.DIRECTEDWEIGHTED);
		
		viewer.displayGraph(graph.getGraph());

		startNode.getItems().clear();
		endNode.getItems().clear();

		startNode.getItems().addAll(graph.getGraph().getNodeSet());
		endNode.getItems().addAll(graph.getGraph().getNodeSet());
	}
	
	@FXML
	void toggleAutoAllignment() {
		if(toggleAllignment) {
			viewer.getViewer().enableAutoLayout();
			toggleAllignment = false;
		} else {
			viewer.getViewer().disableAutoLayout();
			toggleAllignment = true;
		}
	}
}
