package gui.logik;

import java.util.ArrayList;
import java.util.List;

import org.graphstream.graph.Node;

import algorithm.maximumFlow.EdmondsKarp;
import algorithm.maximumFlow.FordFulkerson;
import algorithm.shortesPath.Dijkstra;
import algorithm.shortesPath.FloydWarshall;
import algorithm.traversing.BreadthFirstSearch;
import algorithm.traversing.DepthFirstSearch;
import enums.AlgoType;
import enums.GraphType;
import graph.GraphReader;
import graph.GraphViewer;
import interfaces.AttributeNames;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;

public class GuiPathAlgorithm implements AttributeNames{
	GraphReader graph;
	GraphViewer viewer;
	


	public GuiPathAlgorithm(GraphReader graph, GraphViewer viewer) {
		this.graph = graph;
		this.viewer = viewer;
	}



	public List<Node> runBFS(ChoiceBox<Node> startNode, ChoiceBox<Node> endNode) {
		Node selectedStart = startNode.getSelectionModel().getSelectedItem();
		Node selectedEnd = endNode.getSelectionModel().getSelectedItem();
		
		List<Node> sPath  = new ArrayList<>();
		
		if (hasNodes(selectedStart, selectedEnd)) {
			BreadthFirstSearch graph_bfs = new BreadthFirstSearch(graph.getGraph(), selectedStart, selectedEnd, true);
			graph_bfs.run();

			sPath = graph_bfs.getShortestPath();
			viewer.drawPath(sPath);	
		}
		return sPath;
	}

	public List<Node> runDFS(ChoiceBox<Node> startNode, ChoiceBox<Node> endNode) {
		Node selectedStart = startNode.getSelectionModel().getSelectedItem();
		Node selectedEnd = endNode.getSelectionModel().getSelectedItem();
		
		List<Node> sPath  = new ArrayList<>();
		
		if (hasNodes(selectedStart, selectedEnd)) {
			DepthFirstSearch graph_dfs = new DepthFirstSearch(graph.getGraph(), selectedStart, selectedEnd, true);
			graph_dfs.run();

			sPath = graph_dfs.getShortestPath();
			viewer.drawPath(sPath);	
		}
		return sPath;
	}
	
	public List<Node> runDijkstra(ChoiceBox<Node> startNode, ChoiceBox<Node> endNode) {
		Node selectedStart = startNode.getSelectionModel().getSelectedItem();
		Node selectedEnd = endNode.getSelectionModel().getSelectedItem();

		List<Node> sPath  = new ArrayList<>();
		
		if (hasNodes(selectedStart, selectedEnd)) {
			if(graph.getGraphType() == GraphType.DIRECTEDWEIGHTED || graph.getGraphType() == GraphType.UNDIRECTEDWEIGHTED) { 
				Dijkstra graph_dijkstra = new Dijkstra(graph.getGraph(), selectedStart, selectedEnd);
				graph_dijkstra.run();

				sPath = graph_dijkstra.shortestPath();
				viewer.drawPath(sPath);
			}
		}
		return sPath;
	}
	
	public List<Node> runFloyd(ChoiceBox<Node> startNode, ChoiceBox<Node> endNode) {
		Node selectedStart = startNode.getSelectionModel().getSelectedItem();
		Node selectedEnd = endNode.getSelectionModel().getSelectedItem();
		
		List<Node> sPath  = new ArrayList<>();
		
		if (hasNodes(selectedStart, selectedEnd)) {
			if(graph.getGraphType() == GraphType.DIRECTEDWEIGHTED || graph.getGraphType() == GraphType.UNDIRECTEDWEIGHTED) { 
				FloydWarshall floyd = new FloydWarshall(graph.getGraph(), selectedStart, selectedEnd);
				floyd.run();
				
				sPath = floyd.shortestPath();
				viewer.drawPath(sPath);
			}
		}
		return sPath;
	}
	
	public Double runFord(ChoiceBox<Node> startNode, ChoiceBox<Node> endNode) {
		Node selectedStart = startNode.getSelectionModel().getSelectedItem();
		Node selectedEnd = endNode.getSelectionModel().getSelectedItem();
		
		Double myFlow = Double.NaN;
		
		if (hasNodes(selectedStart, selectedEnd)) {
			if(graph.getGraphType() == GraphType.DIRECTEDWEIGHTED || graph.getGraphType() == GraphType.UNDIRECTEDWEIGHTED) { 
				FordFulkerson fordFulkerson = new FordFulkerson(graph.getGraph(), selectedStart, selectedEnd);
				myFlow = fordFulkerson.run();
			}
		}
		return myFlow;
	}
	
	public Double runEdmondsKarp(ChoiceBox<Node> startNode, ChoiceBox<Node> endNode) {
		Node selectedStart = startNode.getSelectionModel().getSelectedItem();
		Node selectedEnd = endNode.getSelectionModel().getSelectedItem();
		
		Double myFlow = Double.NaN;
		
		if (hasNodes(selectedStart, selectedEnd)) {
			if(graph.getGraphType() == GraphType.DIRECTEDWEIGHTED || graph.getGraphType() == GraphType.UNDIRECTEDWEIGHTED) { 
				EdmondsKarp edmondsKarp = new EdmondsKarp(graph.getGraph(), selectedStart, selectedEnd);
				myFlow = edmondsKarp.run();
			}
		}
		return myFlow;
	}
	
	public void printPathOnGui(List<Node> path, TextArea textArea, AlgoType type) {
		Double lenght = 0.0;
		
		if(path == null || path.isEmpty()) {
			textArea.appendText("Es gibt kein weg zum Ziel \n");
		} else if (type == AlgoType.BFS){
			lenght  = getLenght(path, true);
			textArea.appendText(getString("BFS", lenght, path));
		} else if (type == AlgoType.DFS){
			lenght  = getLenght(path, true);
			textArea.appendText(getString("DFS", lenght, path));
		} else if (type == AlgoType.Dijkstra){
			lenght  = getLenght(path, false);
			textArea.appendText(getString("Dijkstra", lenght, path));
		} else if (type == AlgoType.Floyd){
			lenght  = getLenght(path, false);
			textArea.appendText(getString("Floyd-Warshall", lenght, path));
		} else {
			lenght  = getLenght(path, false);
			textArea.appendText("???: Länge " + lenght + " Pfad: " + path.toString() + " \n");
		}
	}
	
	private String getString(String algoName, Double lenght, List<Node> path) {
		return algoName + ": Länge " + lenght + " Pfad: " + path.toString() + " \n";
	}
	
	private Double getLenght(List<Node> list, boolean nodeCount) {
		Double lenght = 0.0;
		
		if (nodeCount) {
			lenght = (double) list.size() - 1; //Listen Länge minus dem Start Knoten
		} else {
			for (int i = 0; i < list.size()-1; i++) {
				if(list.get(i).hasEdgeToward(list.get(i+1)) && list.get(i).getEdgeToward(list.get(i+1)).hasAttribute(EDGE_WEIGHT)) {
					Double currentLenght = list.get(i).getEdgeToward(list.get(i+1)).getAttribute(EDGE_WEIGHT);
					//System.out.println(currentLenght);
					lenght = lenght + currentLenght;
				} else {
					return Double.NaN;
				}
			}
		}
			
		return lenght; 
	}
	
	private boolean hasNodes(Node selectedStart, Node selectedEnd) {
		if (selectedStart == null || selectedEnd == null) {
			return false;
		} else {
			return true;
		}
	}
}
