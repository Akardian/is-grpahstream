package graph;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.Viewer.CloseFramePolicy;

import interfaces.AttributeNames;

public class GraphViewer implements AttributeNames{
	private final String renderer;
	private final String rendererType;
	private final CloseFramePolicy closePolicy;

	Viewer viewer;

	public GraphViewer() {
		this("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer",
				CloseFramePolicy.CLOSE_VIEWER);
	}

	/**
	 * 
	 * @param renderer
	 * @param rendererType
	 * @param closePolicy
	 */
	public GraphViewer(String renderer, String rendererType, CloseFramePolicy closePolicy) {
		System.setProperty(renderer, rendererType);

		this.renderer = renderer;
		this.rendererType = rendererType;
		this.closePolicy = closePolicy;
	}

	/**
	 * Stellt einen Grahpen in einem Viewer dar
	 * 
	 * @param graph
	 * @return
	 */
	public Viewer displayGraph(Graph graph) {
		// Pfad zum Stylesheet
		String basePath = new File("").getAbsolutePath();
		String path = basePath + "/src/gui/stylesheet.css";

		// Setzt denn "advanced viewer" und die CSS setzen
		graph.addAttribute(UI_STYLESHEET, "url(" + path + ")");

		//updateView(graph);
		
		viewer = graph.display();
		viewer.setCloseFramePolicy(closePolicy);

		return viewer;
	}

	public Viewer getViewer() {
		return viewer;
	}

	public void clean(Graph graph) {
		cleanNodes(graph);
		cleanEdges(graph);
	}
	
	public void cleanNodes(Graph graph) {
		for(Node node : graph) {
			if(node.hasAttribute(NODE_NAME)) {
				node.addAttribute(UI_LABEL, node.getAttribute(NODE_NAME).toString());
			}
			node.addAttribute(UI_CLASS, STYLE_NODE);
		}
	}
	
	public void cleanEdges(Graph graph) {
		 Collection<Edge> edgeIterator = graph.getEdgeSet();
		
		for(Edge edge : edgeIterator) {
			edge.addAttribute(UI_CLASS, STYLE_EDGE);
		}
	}
	
	public void drawPath(List<Node> nodeList) {
		Node lastNode = null;
		
		if(nodeList != null) {
			for (Node node : nodeList) {
				node.addAttribute(UI_CLASS, STYLE_NODE_PATH);
				 
				if(lastNode != null) {
					Edge edge = lastNode.getEdgeToward(node);
					edge.addAttribute(UI_CLASS, STYLE_EDGE_PATH);
				}	 
				lastNode = node;
			}
		}
	}
	
	@Override
	public String toString() {
		return "GraphViewer [renderer=" + renderer + ", rendererType=" + rendererType + ", closePolicy=" + closePolicy
				+ ", viewer=" + viewer + "]";
	}

}
