package graph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;

import enums.GraphType;
import interfaces.AttributeNames;
import interfaces.GraphRegex;

public class GraphReader implements GraphRegex, AttributeNames {

	private Graph graph;
	private GraphType graph_type;
	private boolean graph_error = false;

	public GraphReader() {
		graph = new MultiGraph("graph");
		graph.setStrict(false);
	}

	/**
	 * Lie�ｿｽ�ｽｿ�ｽｽ�ｿｽ�ｽｽ�ｽｾ隹ｿ�ｽｳ die Datei Zeilenweise aus
	 * 
	 * @param file:
	 *            Datei welche dargestellt werden soll
	 */
	public void initGraph(File file) {		
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(file));

			String line = "";
			while ((line = reader.readLine()) != null) {
				if (graph_type == null) {
					// Wenn noch kein GraphType gefunden wurde wird er
					// ermittelt.
					checkGraphTyp(line);
				}

				// Ermittelt die Kanten und Knoten
				checkForEdgesVertex(line);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Pr�ｿｽ�ｽｿ�ｽｽ�ｿｽ�ｽｽ�ｽｿ�ｿｽ�ｽｽ�ｽｽft die Zeile auf einen Graphen Typ
	 * 
	 * @param line:
	 *            Die Zeile welche gepr�ｿｽ�ｽｿ�ｽｽ�ｿｽ�ｽｽ�ｽｿ�ｿｽ�ｽｽ�ｽｽft werden soll
	 * @throws IOException
	 */
	private void checkGraphTyp(String line) throws IOException {
		if (line.matches(directed)) {
			setGraphType(GraphType.DIRECTED);

			if (line.matches(directedWeightedGraph)) {
				setGraphType(GraphType.DIRECTEDWEIGHTED);
			}
		}

		if (line.matches(undirected)) {
			setGraphType(GraphType.UNDIRECTED);

			if (line.matches(undirectedWeightedGraph)) {
				setGraphType(GraphType.UNDIRECTEDWEIGHTED);
			}
		}

		System.out.println("Graph Type: " + this.graph_type);
	}

	/**
	 * Wendet die Regex auf die Zeile an und f�ｿｽ�ｽｿ�ｽｽ�ｿｽ�ｽｽ�ｽｿ�ｿｽ�ｽｽ�ｽｽgt Knoten und Kanten zum Graph
	 * hinzu
	 * 
	 * @param line:
	 *            Die Zeile welche gepr�ｿｽ�ｽｿ�ｽｽ�ｿｽ�ｽｽ�ｽｿ�ｿｽ�ｽｽ�ｽｽft werden soll
	 * @throws IOException
	 */
	private void checkForEdgesVertex(String line) throws IOException {
		Pattern pattern = null;
		Boolean isDirected = false;

		// Comppiled den Pattern abh鬮｣螂�ｽｿ�ｽｽgig vom gefundnen Typ
		if (graph_type != null) {
			switch (graph_type) {
			case DIRECTED:
				pattern = Pattern.compile(directed);
				isDirected = true;
				break;
			case UNDIRECTED:
				pattern = Pattern.compile(undirected);
				break;
			case DIRECTEDWEIGHTED:
				pattern = Pattern.compile(directedWeightedGraph);
				isDirected = true;
				break;
			case UNDIRECTEDWEIGHTED:
				pattern = Pattern.compile(undirectedWeightedGraph);
				break;
			default:
				System.out.println("Der Graph ist nicht richtig");
			}
		}

		if (pattern != null) {
			setGraphAttributes(line, pattern, isDirected);
		} else {
			setNode(line);
		}
	}

	private void setNode(String line) {
		Pattern pattern = Pattern.compile(specialNode);
		Matcher matcher = pattern.matcher(line);

		if (matcher.find()) {
			Node firstNode = graph.addNode(matcher.group(1).trim());
			firstNode.setAttribute(NODE_NAME, matcher.group(1).trim());
			firstNode.addAttribute(UI_LABEL, firstNode.getId());
		} else {
			setGraphError(true);
		}
	}

	/**
	 * F�ｿｽ�ｽｿ�ｽｽ�ｿｽ�ｽｽ�ｽｿ�ｿｽ�ｽｽ�ｽｽght Knoten und Kanten zum Graphen hinzu
	 * 
	 * @param line:
	 *            Die Zeile welche gepr�ｿｽ�ｽｿ�ｽｽ�ｿｽ�ｽｽ�ｽｿ�ｿｽ�ｽｽ�ｽｽft werden soll
	 * @param pattern:
	 *            Das Pattern welches benutzt werden soll
	 * @param directed:
	 *            Ob der graph Directed und UnDirected ist
	 */
	public void setGraphAttributes(String line, Pattern pattern, boolean directed) {
		Matcher matcher = pattern.matcher(line);

		// Pr�ｿｽ�ｽｿ�ｽｽ�ｿｽ�ｽｽ�ｽｿ�ｿｽ�ｽｽ�ｽｽft ob ein Match gefunden werden kann
		if (matcher.find() == true) {
			String node1 = matcher.group(NODE1_GROUPE).trim();
			String node2 = matcher.group(NODE2_GROUPE).trim();
			String name = "";

			// Setzt Namen fals einer Vorhanden
			if (matcher.group(EDGE_NAME_GROUPE) != null) {
				name = matcher.group(EDGE_NAME_GROUPE).trim();
			}

			// Setzte eine Gewichtung fals eine vorhanden ist.
			Double weight = 0.0;
			String weightForGui = "";
			if (graph_type == GraphType.DIRECTEDWEIGHTED || graph_type == GraphType.UNDIRECTEDWEIGHTED) {
				String weightAsString = matcher.group(WEIGHT_GROUPE).trim();

				try {
					weight = Double.parseDouble(weightAsString);
				} catch (Exception e) {
					weight = Double.NaN;
					System.out.println("Could not parse weight");
				}

				if (!weight.isNaN()) {
					weightForGui = "[" + weight + "]";
				}
			}
			
			//Fﾃｼge Attribute zum Graphen hinzu
			String edge_name = node1 + node2;

			// F�ｿｽ�ｽｿ�ｽｽ�ｿｽ�ｽｽ�ｽｿ�ｿｽ�ｽｽ�ｽｽgt Knoten hinzu
			Node firstNode = graph.addNode(node1);
			Node secondNode = graph.addNode(node2);

			// F�ｿｽ�ｽｿ�ｽｽ�ｿｽ�ｽｽ�ｽｿ�ｿｽ�ｽｽ�ｽｽgt Kante hinzu
			if(!firstNode.hasEdgeBetween(secondNode)) {
				Edge edge = graph.addEdge(edge_name, firstNode, secondNode, directed);
			
				// Setzt das Kanten namen
				edge.setAttribute(EDGE_NAME, name);
				firstNode.setAttribute(NODE_NAME, node1);
				secondNode.addAttribute(NODE_NAME, node2);
				
				// Setze Kanten und Knoten Namen f�ｾ�ｽｼr die GUI
				edge.addAttribute(UI_LABEL, name + weightForGui);
				
				firstNode.addAttribute(UI_LABEL, firstNode.getId());
				secondNode.addAttribute(UI_LABEL, secondNode.getId());
	
				// F�ｿｽ�ｽｿ�ｽｽ�ｿｽ�ｽｽ�ｽｿ�ｿｽ�ｽｽ�ｽｽgt das gewicht einer Kante hinzu
				edge.setAttribute(EDGE_WEIGHT, weight);
			}
		} else if (line.matches("\\s*")) {
			System.out.println("leer Zeile");
		} else {
			setNode(line);
		}
	}

	public void saveGraph(File file) {
		Writer fileWriter = null;
		String writeLine = "";
		
		String type = " -- ";
		if(graph_type == GraphType.DIRECTED || graph_type == GraphType.DIRECTEDWEIGHTED ) {
			type = " -> ";
		}

		try {
			fileWriter = new FileWriter(file);
			HashSet<Edge> addedEdges = new HashSet<>();
			HashSet<Node> addedNodes = new HashSet<>();
			HashSet<Node> singleNode = new HashSet<>();
			
			for (Node currentNode : graph) {
				writeLine = "";
				Iterable<Edge> leavingEdges = currentNode.getEachLeavingEdge();

				//System.out.println(currentNode + "|" + currentNode.getDegree());
				//Nur Ausf�ｿｽ�ｽｾ�ｿｽ�ｽｽ�ｽｼhren wenn Kanten Vorhanden sind
				if (currentNode.getDegree() > 0) {
					for (Edge currentEdge : leavingEdges) {
						//System.out.println(currentEdge);
						//Pr�ｿｽ�ｽｾ�ｿｽ�ｽｽ�ｽｼfe ob die Kante nicht schon hinzugef�ｿｽ�ｽｾ�ｿｽ�ｽｽ�ｽｼgt wurde
						if(!addedEdges.contains(currentEdge)) {
							//Erstelle String zum Speichern
							writeLine = currentEdge.getSourceNode().toString(); //Erster Knoten
							writeLine = writeLine + type; //Type Zeichen zwischen Knoten
							writeLine = writeLine + currentEdge.getTargetNode().toString(); //Zweiter Knoten
							
							//Namen der Kante
							String label = currentEdge.getAttribute(EDGE_NAME);
							if(currentEdge.hasAttribute(EDGE_NAME) && !label.equals("")) {
								writeLine = writeLine + label;
							}
							
							//Gewicht der Kante
							if(currentEdge.hasAttribute(EDGE_WEIGHT)) {
								double weight = currentEdge.getAttribute(EDGE_WEIGHT);
								if(weight != 0) {
									writeLine = writeLine + " : " + weight;
								}
							}
							writeLine = writeLine + ";";
							
							//Kanten und Knoten merken damit sie nicht noch mal Hinzugef�ｿｽ�ｽｾ�ｿｽ�ｽｽ�ｽｼgt werden 
							addedEdges.add(currentEdge);
							addedNodes.add(currentNode);
							addedNodes.add(currentEdge.getTargetNode());
							
							fileWriter.write(writeLine);
							fileWriter.append(System.getProperty("line.separator")); // e.g."\n"
						}
					}
				} else {
					//Einzelnen knoten aufgewahren bis zum ende
					singleNode.add(currentNode);
				}
			}
			
			//Pr�ｿｽ�ｽｾ�ｿｽ�ｽｽ�ｽｼfen ob Einzelne Knoten vorhanden sind und wenn am ende der Datei hinzuf�ｿｽ�ｽｾ�ｿｽ�ｽｽ�ｽｼgen
			for(Node currentNode: singleNode) {
				if(!addedNodes.contains(currentNode)) {
					writeLine = currentNode + ";";
					
					fileWriter.write(writeLine);
					fileWriter.append(System.getProperty("line.separator")); // e.g."\n"
				}
			}
			
		} catch (IOException e) {
			System.err.println("Konnte Datei nicht erstellen");
		} finally {
			if (fileWriter != null)
				try {
					fileWriter.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	public String getAbsolutePath(String path) {
		String basePath = new File("").getAbsolutePath();

		return basePath + path;
	}

	public Graph getGraph() {
		return graph;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
	}

	public GraphType getGraphType() {
		return graph_type;
	}

	public void setGraphType(GraphType graph_type) {
		this.graph_type = graph_type;
	}

	public boolean isGraphError() {
		return graph_error;
	}

	public void setGraphError(boolean graph_error) {
		this.graph_error = graph_error;
	}

}
