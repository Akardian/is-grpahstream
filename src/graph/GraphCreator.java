package graph;

import java.io.File;
import java.util.Random;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;

import enums.GraphType;
import interfaces.AttributeNames;

public class GraphCreator implements AttributeNames{
	public final String NODE_NAME_PRE = "N";
	public final String EDGE_NAME_PRE = "E";
	
	public int numberOfNodes;
	public final int numberOfEdges;
	
	public final int maxWeight;
	public final GraphType graphType;
	
	private int startNodeName;
	private int startEdgeName;
	
	public GraphCreator(int numberOfNodes, int numberOfEdges, GraphType graphType, int maxWeight) {
		this.numberOfNodes = numberOfNodes;
		this.numberOfEdges = numberOfEdges;
		
		this.graphType = graphType;
		this.maxWeight = maxWeight;
		
		this.startNodeName = 1;
		this.startEdgeName = 1;
	}
	
	public Graph generateRandomGraph() {
		Graph graph = new MultiGraph("graph");
		
		generateNodes(graph);
		generateEdges(graph);
		
		return graph;
	}
	
	public Graph generateRandomGraphAndSave() {
		Graph graph = generateRandomGraph();
		
		GraphReader saveGraphe = new GraphReader();
		saveGraphe.setGraph(graph);
		saveGraphe.setGraphType(GraphType.DIRECTEDWEIGHTED);
		
		File file = new File("superBig.gka");
		saveGraphe.saveGraph(file);
		
		return graph;
	}
	
	public Graph generateRandomNetwork() {
		Graph graph = new MultiGraph("graph");
		
		Node nodeS = graph.addNode("s");
		nodeS.addAttribute("s");
		nodeS.addAttribute("s");
		
		Node nodeQ = graph.addNode("q");
		nodeQ.addAttribute("q");
		nodeQ.addAttribute("q");
		
		numberOfNodes = numberOfNodes -2;
		
		generateNodes(graph);
		generateEdges(graph);
		
		return graph;
	}
	
	private void generateEdges(Graph graph) {		
		Random randomGenerator = new Random();
		
		for(int i = 0; i < numberOfEdges; i++) {
			int nodeNumber1 = randomGenerator.nextInt(numberOfNodes);
			int nodeNumber2 = randomGenerator.nextInt(numberOfNodes);
			
			Node node1 = graph.getNode(nodeNumber1);
			Node node2 = graph.getNode(nodeNumber2);
			
			Edge edge = ceateEdge(graph, node1, node2);
			
			if(edge != null && maxWeight > 0) {
				Double weigth = (double) randomGenerator.nextInt(maxWeight);			

				edge.addAttribute(EDGE_WEIGHT, weigth);
				edge.addAttribute(UI_LABEL, "[" + weigth + "]");
			}
			startEdgeName++;
			
		}
	}
	
	private Edge ceateEdge(Graph graph, Node node1, Node node2) {
		 Edge edge = null;
		 
		if(!node1.hasEdgeBetween(node2) && !node1.equals(node2)) {
			if(graphType == GraphType.DIRECTED || graphType == GraphType.DIRECTEDWEIGHTED) {
				edge = graph.addEdge(NODE_NAME_PRE + startEdgeName, node1, node2, true);
			} else if(graphType == GraphType.UNDIRECTED || graphType == GraphType.UNDIRECTEDWEIGHTED){
				edge= graph.addEdge(NODE_NAME_PRE + startEdgeName, node1, node2, false);
			} else {
				System.out.println("Kein GraphType vorhanden");
				edge = null;
			}
		}
		return edge;
	}
	
	/**
	 * Erzeugt Knoten in dem Übegebenen Graphen
	 * @param graph
	 * @param numberOfNodes
	 * @param startNodeName
	 */
	private void generateNodes(Graph graph ) {		
		for(int i = 0; i < numberOfNodes; i++) {
			Node node = graph.addNode(NODE_NAME_PRE + startNodeName);
			node.addAttribute(NODE_NAME, NODE_NAME_PRE + startNodeName);
			node.addAttribute(UI_LABEL, NODE_NAME_PRE + startNodeName);
			
			startNodeName++;
		}
	}
}
